// import { catchError } from 'rxjs/operators';
import { HttpService } from './../../services/http.service';
import { Component, OnInit, OnChanges } from '@angular/core';
import {
  FormControl,
  FormGroupDirective,
  NgForm, Validators,
  FormGroup,
  FormBuilder,
  AbstractControl,
  ValidatorFn
} from '@angular/forms';
// import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';

export function patternValidator(regexp: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const value = control.value;
    if (value === '') {
      return null;
    }
    return !regexp.test(value) ? { 'patternInvalid': { regexp } } : null;
  };
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnChanges {

  loginForm: FormGroup;

  loading = false;
  error = '';

  hide = true;


  constructor(
    private router: Router,
    private authService: HttpService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  ngOnChanges() {
    this.rebuildForm();
  }
  rebuildForm() {
    this.loginForm.reset({
      email: '',
      password: ''
    });
  }

  private createForm() {
    this.loginForm = new FormGroup({
      // tslint:disable-next-line
      email: new FormControl('', [Validators.required, patternValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      password: new FormControl('', Validators.required),
    });
  }

  login(loginForm) {
    console.log(loginForm);
    let promise = new Promise((resolve, reject) => {
      this.loading = true;
      this.authService.login(loginForm)
        .then(
          response => {
            this.router.navigate(['/']);
            resolve();
          },
          msg => {
            this.error = msg;
            reject(msg);
            this.loading = false;
          }
        );
    });
    return promise;
    // this.loading = true;
    // this.authService.login(loginForm)
    //   .subscribe(response => {
    //     if (response === true) {
    //       this.router.navigate(['/']);
    //     } else {
    //       this.error = 'Email or password is incorrect';
    //       this.loading = false;
    //     }
    //   });
  }


}
