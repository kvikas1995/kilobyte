import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { localNetwork } from './../../environments/environment';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  userData;
  // getting component data
  private contentData = new BehaviorSubject<string>('default data');
  currentData = this.contentData.asObservable();

  // getting all errors respond from backend
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.message}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }


  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    // set token if saved in local storage
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  login(data) {
    let promise = new Promise((resolve, reject) => {
      this.http.post(`${localNetwork.apiUrl}/auth/login`, data)
        // .pipe(map((response) => {
        //   window.localStorage.setItem('currentUser', JSON.stringify(response['data']));
        //   if (response['data,token']) {
        //     return true;
        //   } else {
        //     return false;
        //   }
        // }), retry(3), catchError(this.handleError));
        .toPromise()
        .then(
          res => {
            window.localStorage.setItem('currentUser', JSON.stringify(res['data']));
            console.log(res);
            resolve();
          },
          error => {
            reject(error);
          }
        );
    });
    return promise;
  }

  getData(data: any) {
    this.contentData.next(data);
  }



  getBooks() {
    this.userData = JSON.parse(localStorage.getItem('currentUser'));
    let token = 'Bearer' + ' ' + this.userData.token;
    let head = new HttpHeaders({
      authorization: token
    });
    return this.http.get(`${localNetwork.apiUrl}/user/getbooks`, { headers: head });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    window.localStorage.removeItem('currentUser');
    window.localStorage.removeItem('token');
    this.router.navigateByUrl('/login');

  }
}
