import { CartComponent } from './../entryComponent/cart/cart.component';
import { HttpService } from './../../services/http.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  books;
  bookData;
  count = 0;
  values = '';

  constructor(
    private http: HttpService,
    public dialog: MatDialog
  ) {
    this.http.getBooks().subscribe(result => {
      console.log(result); this.books = result['data'];
    });
    // console.log(this.books);
  }

  ngOnInit() {
    // this.http.currentData.subscribe(data => this.count = data);
  }

  data(data: any) {
    if (data === ' ') {
      return ('Your Cart is empty! Please Select some items');
    } else {
      for (let i = 0; i < data.length; i++) {
        // this.bookData.push(data[i]);
        this.count = i++;
      }
      this.bookData = data;
      console.log(this.bookData);
      // console.log(this.count);
      this.http.getData(this.count + 1);
    }
  }


  onKey(event: any) { // without type info
    this.values += event.target.value + ' | ';
  }


  details() {
    const modelRef = this.dialog.open(CartComponent, {
      height: '460px',
      width: '600px'
    });
  }

  nextData() {
    let bookData = this.bookData[this.count++];
    if (this.count >= this.bookData.length) { this.count = 0; }
  }

}
