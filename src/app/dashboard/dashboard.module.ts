import { MaterialModule } from './modules/material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountComponent } from './account/account.component';
import { CartComponent } from './entryComponent/cart/cart.component';
import { HomeComponent } from './home/home.component';
import { KeysPipe } from './keys.pipe';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule
  ],
  declarations: [DashboardComponent, AccountComponent, CartComponent, HomeComponent, KeysPipe],
  entryComponents: [
    CartComponent
  ]
})
export class DashboardModule { }
