import { CartComponent } from './../entryComponent/cart/cart.component';
import { HttpService } from './../../services/http.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  currentUser;
  counter;
  constructor(
    private http: HttpService,
    public dialog: MatDialog
  ) {
    this.currentUser = JSON.parse(window.localStorage.getItem('currentUser'));
    // console.log(this.currentUser);
  }

  ngOnInit() {
    this.http.currentData.subscribe(data => {
      this.counter = data;
    });
  }

  details() {
    const modelRef = this.dialog.open(CartComponent, {
      height: '460px',
      width: '600px'
    });
  }

}
